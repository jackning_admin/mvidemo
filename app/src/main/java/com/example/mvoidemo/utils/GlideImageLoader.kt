package com.example.mvoidemo.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.bumptech.glide.request.target.Target

/**
 * @auth: njb
 * @date: 2024/8/17 15:39
 * @desc: 描述
 */
object GlideImageLoader {
    // 在指定的ImageView中加载图片
    @SuppressLint("CheckResult")
    suspend fun loadImage(
        context: Context,
        url: String,
        imageView: ImageView,
        placeholder: Drawable? = null,
        errorDrawable: Drawable? = null,
        width: Int = ViewGroup.LayoutParams.WRAP_CONTENT,
        height: Int = ViewGroup.LayoutParams.WRAP_CONTENT,
        applyCenterCrop: Boolean = false // 是否应用centerCrop
    ){
        withContext(Dispatchers.Main) {
            val request = Glide.with(context)
                .load(url)
                .placeholder(placeholder)
                .error(errorDrawable)
                .override(width, height) // 指定图片大小
            // 如果需要裁剪，则应用centerCrop
            if(applyCenterCrop){
                request.centerCrop()
            }
            request.into(imageView)
        }
    }


    // 加载图片并返回Drawable对象
    suspend fun loadImageAsDrawable(
        context: Context,
        url: String,
        placeholder: Drawable? = null,
        errorDrawable: Drawable? = null
    ): Drawable? {
        return withContext(Dispatchers.IO) {
            try {
                val request = Glide.with(context)
                    .load(url)
                    .placeholder(placeholder)
                    .error(errorDrawable)
                request.submit().get()
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }

    // 加载图片并在加载完成后执行回调
    suspend fun loadImageWithCallback(
        context: Context,
        url: String,
        onSuccess: (Drawable) -> Unit,
        onError: (() -> Unit)? = null,
        placeholder: Drawable? = null,
        errorDrawable: Drawable? = null
    ) {
        withContext(Dispatchers.Main) {
            Glide.with(context)
                .load(url)
                .placeholder(placeholder)
                .error(errorDrawable)
                .into(object : CustomTarget<Drawable>() {
                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        onSuccess(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // 可选：在图片被清除时执行的操作
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        onError?.invoke()
                    }
                })
        }
    }
}