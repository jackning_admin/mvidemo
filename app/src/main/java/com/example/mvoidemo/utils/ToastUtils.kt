package com.example.mvoidemo.utils

import android.content.Context
import android.widget.Toast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * @auth: njb
 * @date: 2024/8/22 23:52
 * @desc: 描述
 */
object ToastUtils {
    private var toastJob : Job?= null

    fun Context.showToast(context: Context,msg:String,showType:Int){
        toastJob?.cancel()
        toastJob = CoroutineScope(Dispatchers.Main).launch{
            Toast.makeText(context,msg,showType).show()
            //延迟2秒防止2s内快速点击弹出toast
            delay(2000)
        }
    }
}