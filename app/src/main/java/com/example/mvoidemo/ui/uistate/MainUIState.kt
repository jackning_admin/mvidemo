package com.example.mvoidemo.ui.uistate

import com.example.mvoidemo.model.ArticleListModel
import com.example.mvoidemo.base.BaseModel
import com.example.mvoidemo.model.BannerModel

/**
 * @auth: njb
 * @date: 2024/8/13 17:35
 * @desc: 描述
 */
sealed class MainUIState {
    data object IDle: MainUIState()

    data object Loading: MainUIState()

    data class onSuccess(val result:BaseModel<ArticleListModel>) : MainUIState()

    data class onError(val errorMsg:String) : MainUIState()

    data class onBanner(val bannerList:BaseModel<List<BannerModel>>) : MainUIState()
}