package com.example.mvoidemo.ui.viewmodel

import android.widget.AbsListView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvoidemo.http.ApiService
import com.example.mvoidemo.ui.MainRepository

/**
 * @auth: njb
 * @date: 2024/8/15 23:40
 * @desc: 描述
 */
class ViewModelFactory(private val apiService: ApiService):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        // 判断 MainViewModel 是不是 modelClass 的父类或接口
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainRepository(apiService)) as T
        }
        throw IllegalArgumentException("UnKnown class")
    }
}