package com.example.mvoidemo.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvoidemo.intent.MainIntent
import com.example.mvoidemo.ui.MainRepository
import com.example.mvoidemo.ui.uistate.MainUIState
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch

/**
 * @auth: njb
 * @date: 2024/8/12 2:19
 * @desc: 描述
 */
class MainViewModel(private val repository: MainRepository) :ViewModel(){
    //创建意图管道，容量无限大
    val mainIntentChannel = Channel<MainIntent>(Channel.UNLIMITED)

    //可变状态数据流
    private val _state = MutableStateFlow<MainUIState>(MainUIState.IDle)

    //可观察状态数据流
    val state: StateFlow<MainUIState> get() = _state

    init {
        viewModelScope.launch {
            //收集意图
            mainIntentChannel.consumeAsFlow().collect {
                when (it) {
                    //发现意图为获取壁纸
                    is MainIntent.getArticle -> getArticleList(1)

                    is MainIntent.getBanner -> getBanner()
                }
            }
        }
    }

    /**
     * 获取壁纸
     */
    private fun getArticleList(page:Int) {
        viewModelScope.launch {
            //修改状态为加载中
            _state.value = MainUIState.Loading
            //网络请求状态
            _state.value = try {
                //请求成功
                MainUIState.onSuccess(repository.getArticle(page))
            } catch (e: Exception) {
                //请求失败
                MainUIState.onError(e.localizedMessage ?: "UnKnown Error")
            }
        }
    }

    private fun getBanner(){
        viewModelScope.launch {
            _state.value = MainUIState.Loading
            //网络请求状态
            _state.value = try {
                //请求成功
                MainUIState.onBanner(repository.getBanner())
            } catch (e: Exception) {
                //请求失败
                MainUIState.onError(e.localizedMessage ?: "UnKnown Error")
            }
        }
    }
}
