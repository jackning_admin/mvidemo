package com.example.mvoidemo.ui

import com.example.mvoidemo.http.ApiService

/**
 * @auth: njb
 * @date: 2024/8/15 23:39
 * @desc: 描述
 */
class MainRepository(private val apiService: ApiService){
    suspend fun getArticle(page:Int) = apiService.getArticleList(page)

    suspend fun getBanner() = apiService.getBanner()
}