package com.example.mvoidemo.constants

/**
 * @auth: njb
 * @date: 2024/8/19 1:45
 * @desc: 描述
 */
object Constants {
    const val IMG_URL = "https://www.wanandroid.com/blogimgs/50c115c2-cf6c-4802-aa7b-a4334de444cd.png"
    //const val IMG_URL = "https://wxls-cms.oss-cn-hangzhou.aliyuncs.com/online/2024-04-18/218da022-f4bf-456a-99af-5cb8e157f7b8.jpg"
}