package com.example.mvoidemo

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.blankj.utilcode.util.GsonUtils
import com.example.mvoidemo.constants.Constants
import com.example.mvoidemo.databinding.ActivityMainBinding
import com.example.mvoidemo.http.HttpUtils
import com.example.mvoidemo.intent.MainIntent
import com.example.mvoidemo.ui.uistate.MainUIState
import com.example.mvoidemo.ui.viewmodel.MainViewModel
import com.example.mvoidemo.ui.viewmodel.ViewModelFactory
import com.example.mvoidemo.utils.GlideImageLoader
import com.example.mvoidemo.utils.ToastUtils
import com.example.mvoidemo.utils.ToastUtils.showToast
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val TAG = "okhttp"
    private lateinit var mainViewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        initData()
        initView()
        initImage()
    }

    private fun initView() {
        lifecycleScope.launch{
            mainViewModel.mainIntentChannel.send(MainIntent.getArticle)
            mainViewModel.mainIntentChannel.send(MainIntent.getBanner)
        }
        binding.cb.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                buttonView.background = ContextCompat.getDrawable(this,R.drawable.baseline_check_circle_24)
            }else{
                buttonView.background = ContextCompat.getDrawable(this,R.drawable.baseline_radio_button_unchecked_24)
            }
        }
    }

    private fun initImage() {
        lifecycleScope.launch {
            GlideImageLoader.loadImage(this@MainActivity,Constants.IMG_URL,binding.imageview,null,null,800,400,true)
        }
    }

    private fun initData() {
        mainViewModel = ViewModelProvider(
            this,
            ViewModelFactory(HttpUtils.apiService)
        )[MainViewModel::class.java]
        lifecycleScope.launch {
            mainViewModel.state.collect {
                when(it){
                    is MainUIState.IDle ->{}
                    is MainUIState.Loading ->{}
                    is MainUIState.onSuccess ->{
                        it.result.data?.datas?.forEach {
                            binding.textview.text = it.chapterName
                            Log.d(TAG,GsonUtils.toJson(it.chapterName))
                        }
                    }
                    is MainUIState.onError ->{
                        Log.d("TAG", "observeViewModel: $it.error")
                      //  ToastUtils.showToast(this@MainActivity,it.errorMsg,0)
                    }
                    is MainUIState.onBanner ->{
                        it.bannerList.data?.forEach {
                            Log.d(TAG,"===图片地址为==="+GsonUtils.toJson(it.imagePath))
                        }
                    }
                }
            }
        }
    }
}