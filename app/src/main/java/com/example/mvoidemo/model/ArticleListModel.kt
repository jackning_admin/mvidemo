package com.example.mvoidemo.model

import com.squareup.moshi.Json
import java.io.Serializable

/**
 * @auth: njb
 * @date: 2024/8/15 23:33
 * @desc: 描述
 */
data class ArticleListModel(
    @Json(name = "datas")
    var datas:List<ArticleModel>
):Serializable

data class ArticleModel(
    @Json(name = "apkLink")
    var apkLink: String? = "" ,        // 文章uri
    @Json(name = "author")
    var author: String? = "" ,         //
    @Json(name = "chapterId")
    var chapterId: Int  ,           //
    @Json(name = "chapterName")
    var chapterName: String? = "",
    @Json(name = "isCollect")
    var isCollect: Boolean = false,
    @Json(name = "courseId")
    var courseId: Int,
    @Json(name = "desc")
    var desc: String? = "",
    @Json(name = "envelopePic")
    var envelopePic: String? = "",
    @Json(name = "isFresh")
    var isFresh: Boolean = false,
    @Json(name = "id")
    var id: Int,
    @Json(name = "link")
    var link: String? = "",
    @Json(name = "niceDate")
    var niceDate: String? = "",
    @Json(name = "origin")
    var origin: String? = "",
    @Json(name = "projectLink")
    var projectLink: String? = "",
    @Json(name = "publishTime")
    var publishTime: Long,
    @Json(name = "superChapterId")
    var superChapterId: Int,
    @Json(name = "superChapterName")
    var superChapterName: String? = "",
    @Json(name = "title")
    var title: String? = "",
    @Json(name = "type")
    var type: Int,
    @Json(name = "userId")
    var userId: Int,
    @Json(name = "visible")
    var visible: Int,
    @Json(name = "zan")
    var zan: Int,
    @Json(name = "isSelect")
    var isSelect: Boolean
):Serializable