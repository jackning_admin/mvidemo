package com.example.mvoidemo.model

import java.io.Serializable
import com.squareup.moshi.Json

/**
 * @auth: njb
 * @date: 2024/8/19 1:34
 * @desc: 描述
 */
data class BannerModel(
    @Json(name = "desc")
    var desc: String,
    @Json(name = "id")
    var id: Int,
    @Json(name = "imagePath")
    var imagePath: String,
    @Json(name = "isVisible")
    var isVisible: Int,
    @Json(name = "order")
    var order: Int,
    @Json(name = "title")
    var title: String,
    @Json(name = "type")
    var type: Int,
    @Json(name = "url")
    var url: String,
):Serializable