package com.example.mvoidemo.http

import com.example.mvoidemo.model.ArticleListModel
import com.example.mvoidemo.base.BaseModel
import com.example.mvoidemo.model.BannerModel
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @auth: njb
 * @date: 2024/8/4 23:45
 * @desc: 描述
 */
interface ApiService {
    @GET("/article/list/{page}/json")
    suspend fun getArticleList(@Path("page") page: Int):BaseModel<ArticleListModel>

    @GET("/banner/json")
    suspend fun getBanner():BaseModel<List<BannerModel>>

    @GET("/hotkey/json")
    suspend fun getHotKey():BaseModel<ArticleListModel>

    @GET("/tree/json")
    suspend fun getTree():BaseModel<ArticleListModel>

    @GET("/navi/json")
    suspend fun getNavi():BaseModel<ArticleListModel>

    @GET("/project/tree/json")
    suspend fun getProject():BaseModel<ArticleListModel>

    @GET("/friend/json")
    suspend fun getFriend():BaseModel<ArticleListModel>
}