package com.example.mvoidemo.intent

/**
 * @auth: njb
 * @date: 2024/8/15 23:22
 * @desc: 描述
 */
sealed class MainIntent {
    data  object getArticle : MainIntent()

    data object getBanner : MainIntent()
}