package com.example.mvoidemo.base

import com.squareup.moshi.Json
import java.io.Serializable

/**
 * @auth: njb
 * @date: 2024/8/15 23:26
 * @desc: 描述
 */
data class BaseModel<T>(
        @Json(name = "data")
        var data:T ?= null,
        @Json(name = "errorCode")
        var errorCode:Int,
        @Json(name = "errorMsg")
        var errorMsg:String = ""
):Serializable